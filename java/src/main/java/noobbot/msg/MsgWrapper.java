package noobbot.msg;


public class MsgWrapper {
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}

	@Override
	public String toString() {
		return "MsgWrapper [msgType=" + msgType + ", data=" + data + "]";
	}
}