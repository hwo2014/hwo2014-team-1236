package noobbot.handler;

import java.util.ArrayList;
import java.util.List;

import noobbot.MsgSendable;
import noobbot.model.CarPositions;
import noobbot.model.CarPositionsData;
import noobbot.model.CarStatus;
import noobbot.model.GameInit;
import noobbot.model.Piece;
import noobbot.model.PieceTypeEnum;
import noobbot.model.PlayerId;
import noobbot.model.Track;
import noobbot.model.YourCar;
import noobbot.msg.MsgWrapper;
import noobbot.msg.Ping;
import noobbot.msg.Throttle;
import noobbot.util.MsgDataUtil;

public enum MsgHandlerEnum implements MsgHandler {
	CAR_POSITIONS("carPositions") {
		@Override
		public void doHandle(MsgWrapper wrapper, MsgSendable sender) {
			PlayerId myCarInfo = sender.getMyCar();
			CarPositions carPositions = MsgDataUtil.parseData(wrapper,
					CarPositions.class);
			CarPositionsData myPos = findMyCarPos(myCarInfo, carPositions);
			if (myPos == null) {
				// TODO : Error
				System.err.print("error");
				sender.send(new Ping());
				return;
			}

			sender.getStatus().currentCarPos = myPos.piecePosition;

			float v = calcVelocity(sender);
			sender.getStatus().currentVelocity = v;

			Track track = sender.getTrack();
			CarStatus status = sender.getStatus();
			if (Math.abs(myPos.angle) > 10) {
				status.decreaseThrottle(1.0f);
			} else {
				PieceTypeEnum type = findNextPieceTypeEnum(myPos, track);
				switch (type) {
				case CURVE:
					if (v > 7) {
						status.decreaseThrottle(0.3f);
					} else {
						status.increaseThrottle(0.05f);
					}
					break;
				case SHARP_CURVE:
					if (v > 5) {
						status.decreaseThrottle(0.3f);
					} else {
						status.increaseThrottle(0.05f);
					}
					break;
				case STRAIGHT:
					if (v > 8) {
						status.decreaseThrottle(0.3f);
					} else {
						status.increaseThrottle(0.05f);
					}
					break;
				}
			}
			sender.send(new Throttle(status.throttleValue));
			sender.getStatus().beforeCarPos = sender.getStatus().currentCarPos;
		}

		private PieceTypeEnum findNextPieceTypeEnum(CarPositionsData myPos,
				Track track) {
			int toIndex = myPos.piecePosition.pieceIndex + 6;
			if (toIndex > track.pieces.size()) {
				toIndex = track.pieces.size();
			}
			List<Piece> list = new ArrayList<Piece>();
			list.addAll(track.pieces.subList(myPos.piecePosition.pieceIndex,
					toIndex));
			if (toIndex == track.pieces.size()) {
				list.addAll(track.pieces.subList(0, 6));
			}
			List<Piece> prevList = list.subList(0, Math.min(list.size(), 6));
			PieceTypeEnum type = PieceTypeEnum.STRAIGHT;
			for (Piece item : prevList) {
				if (item.angle != null) {
					if (item.angle < 5) {
						type = PieceTypeEnum.STRAIGHT;
					} else if (item.angle < 15) {
						type = PieceTypeEnum.CURVE;
						break;
					} else if (item.angle < 30) {
						type = PieceTypeEnum.SHARP_CURVE;
						break;
					}
				} else {
					type = PieceTypeEnum.STRAIGHT;
					break;
				}
			}
			return type;
		}

		private float calcVelocity(MsgSendable sender) {
			float beforeDistance = 0.0f;
			if (sender.getStatus().beforeCarPos != null) {
				beforeDistance = 100 * sender.getStatus().beforeCarPos.lap
						* sender.getTrack().pieces.size();
				beforeDistance += 100 * sender.getStatus().beforeCarPos.pieceIndex;
				beforeDistance += sender.getStatus().beforeCarPos.inPieceDistance;
			}

			float currentDistnace = 0.0f;
			currentDistnace = 100 * sender.getStatus().currentCarPos.lap
					* sender.getTrack().pieces.size();
			currentDistnace += 100 * sender.getStatus().currentCarPos.pieceIndex;
			currentDistnace += sender.getStatus().currentCarPos.inPieceDistance;
			int tick = 1;
			float v = (currentDistnace - beforeDistance) / tick;
			return v;
		}

		private CarPositionsData findMyCarPos(PlayerId myCarInfo,
				CarPositions carPositions) {
			for (CarPositionsData item : carPositions.data) {
				if (item.id.equals(myCarInfo)) {
					return item;
				}
			}
			return null;
		}
	},

	JOIN("join") {
		@Override
		public void doHandle(MsgWrapper wrapper, MsgSendable sender) {
			System.out.println("Joined");
		}
	},
	GAME_INIT("gameInit") {
		@Override
		public void doHandle(MsgWrapper wrapper, MsgSendable sender) {
			GameInit initInfo = MsgDataUtil.parseData(wrapper, GameInit.class);
			sender.init(initInfo);
			System.out.println("Race init");
		}
	},
	GAME_END("gameEnd") {
		@Override
		public void doHandle(MsgWrapper wrapper, MsgSendable sender) {
			sender.send(new Ping());
			System.out.println("Race end");
		}
	},
	GAME_START("gameStart") {
		@Override
		public void doHandle(MsgWrapper wrapper, MsgSendable sender) {
			System.out.println("Race start");
		}
	},
	YOUR_CAR("yourCar") {
		@Override
		public void doHandle(MsgWrapper wrapper, MsgSendable msgSender) {
			YourCar myCar = MsgDataUtil.parseData(wrapper, YourCar.class);
			msgSender.setMyCar(myCar.data);
		}

	},
	CRASH("crash") {
		@Override
		public void doHandle(MsgWrapper wrapper, MsgSendable msgSender) {
			float v = msgSender.getStatus().currentVelocity;
			Piece p = msgSender.getTrack().pieces
					.get(msgSender.getStatus().currentCarPos.pieceIndex);

			System.err.printf("crash Info : V : %s, path : %s\n", v,
					p.toString());
			msgSender.send(new Ping());
		}
	},
	SPWAN("spawn") {
		@Override
		public void doHandle(MsgWrapper wrapper, MsgSendable msgSender) {
		}

	},
	DEFAULT(null) {
		@Override
		public void doHandle(MsgWrapper wrapper, MsgSendable sender) {
			sender.send(new Ping());
		}
	};
	private final String msgType;

	private MsgHandlerEnum(String msgType) {
		this.msgType = msgType;
	}

	public static MsgHandlerEnum findBy(String msgType) {
		for (MsgHandlerEnum item : values()) {
			if (item.msgType.equals(msgType)) {
				return item;
			}
		}
		return MsgHandlerEnum.DEFAULT;
	}
}