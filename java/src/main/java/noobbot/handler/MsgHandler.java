package noobbot.handler;

import noobbot.MsgSendable;
import noobbot.msg.MsgWrapper;

public interface MsgHandler {
	void doHandle(MsgWrapper wrapper, MsgSendable msgSender);
}
