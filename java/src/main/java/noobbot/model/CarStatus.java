package noobbot.model;

public class CarStatus {
	public float throttleValue = 0.5f;
	public PiecePosition beforeCarPos;
	public PiecePosition currentCarPos;
	public float currentVelocity;

	public int getVelocity() {
		return 0;
	}

	public void increaseThrottle(float value) {
		throttleValue += value;
		if (throttleValue > 1.0) {
			throttleValue = 1.0f;
		}
	}

	public void decreaseThrottle(float value) {
		throttleValue -= value;
		if (throttleValue < 0) {
			throttleValue = 0;
		}
	}
}
