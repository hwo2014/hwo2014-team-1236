package noobbot.model;


/**
 * car position message
 * @author hothead
 */
public class CarPositionsData {
	public PlayerId id;
	public float angle;
	public PiecePosition piecePosition;
}
