package noobbot.model;

import java.io.Serializable;
import java.util.List;

public class CarPositions implements Serializable {
	private static final long serialVersionUID = 8766105144968951820L;
	public String msgType;
	public List<CarPositionsData> data;
}
