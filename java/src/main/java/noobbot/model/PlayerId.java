package noobbot.model;

public class PlayerId {
	public String name;
	public String color;

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PlayerId == false) {
			return false;
		}
		if (this.name.equals(((PlayerId) obj).name)
				&& this.color.equals(((PlayerId) obj).color)) {
			return true;
		}
		
		return false;
	}
}
