package noobbot.model;


public class PiecePosition {
	public int pieceIndex;
	public float inPieceDistance;
	public Lane lane;
	public int lap;
}
