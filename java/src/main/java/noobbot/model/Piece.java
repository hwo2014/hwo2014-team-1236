package noobbot.model;

public class Piece {
	public Float length;
	private Boolean switchable;
	public Float radious;
	public Float angle;

	public Boolean getSwitch() {
		return switchable;
	}

	public void setSwitch(Boolean switchable) {
		this.switchable = switchable;
	}

	@Override
	public String toString() {
		return "Piece [length=" + length + ", switchable=" + switchable
				+ ", radious=" + radious + ", angle=" + angle + "]";
	}
}
