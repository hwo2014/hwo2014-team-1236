package noobbot.model;

public enum PieceTypeEnum {
	STRAIGHT, SHARP_CURVE, CURVE;
}
