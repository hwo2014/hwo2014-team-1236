package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import noobbot.handler.MsgHandlerEnum;
import noobbot.model.CarStatus;
import noobbot.model.GameInit;
import noobbot.model.PlayerId;
import noobbot.model.Track;
import noobbot.msg.Join;
import noobbot.msg.MsgWrapper;
import noobbot.msg.SendMsg;

import com.google.gson.Gson;

public class Main implements MsgSendable {
	private Join join;
	private CarStatus status;
	private GameInit initInfo;
	private PlayerId myCar;

	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

//		String host = "testserver.helloworldopen.com";
//		int port = 8091;
//		String botName = "WENDIES";
//		String botKey = "CmFz4c1ZN6Lhzw";

		System.out.println("Connecting to " + host + ":" + port + " as "
				+ botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(
				socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				socket.getInputStream(), "utf-8"));
		new Main(reader, writer, new Join(botName, botKey));
	}

	final Gson gson = new Gson();
	private PrintWriter writer;

	public Main(final BufferedReader reader, final PrintWriter writer,
			final Join join) throws IOException {
		this.writer = writer;
		this.join = join;
		this.status = new CarStatus();
		String line = null;

		send(join);

		while ((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line,
					MsgWrapper.class);
			System.out.println("[recv] : " + msgFromServer + "\n, velocity : + " + getStatus().currentVelocity);
			MsgHandlerEnum handler = MsgHandlerEnum
					.findBy(msgFromServer.msgType);
			
			handler.doHandle(msgFromServer, this);
		}
	}

	@Override
	public void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}

	@Override
	public Join getJoin() {
		return this.join;
	}

	@Override
	public CarStatus getStatus() {
		return this.status;
	}

	@Override
	public void init(GameInit initInfo) {
		this.initInfo = initInfo;
	}

	@Override
	public void setMyCar(PlayerId myCar) {
		this.myCar = myCar;
	}

	@Override
	public PlayerId getMyCar() {
		return this.myCar;
	}

	@Override
	public Track getTrack() {
		return this.initInfo.data.race.track;
	}
}