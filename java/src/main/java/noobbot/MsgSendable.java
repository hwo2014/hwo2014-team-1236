package noobbot;

import noobbot.model.CarStatus;
import noobbot.model.GameInit;
import noobbot.model.PlayerId;
import noobbot.model.Track;
import noobbot.msg.Join;
import noobbot.msg.SendMsg;

public interface MsgSendable {
	public void send(SendMsg msg);
	public Join getJoin();
	public CarStatus getStatus();
	public void init(GameInit initInfo);
	public void setMyCar(PlayerId myCar);
	public PlayerId getMyCar();
	public Track getTrack();
}
