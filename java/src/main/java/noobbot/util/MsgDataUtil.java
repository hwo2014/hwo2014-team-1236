package noobbot.util;

import noobbot.msg.MsgWrapper;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

public class MsgDataUtil {
	public static <T> T parseData(MsgWrapper msg, Class<T> clazz) {
		Gson gson = new Gson();
		JsonElement elem = gson.toJsonTree(msg);
		return gson.fromJson(elem, clazz);
	}
}
